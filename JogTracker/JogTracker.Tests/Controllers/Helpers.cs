﻿using JogTracker.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace JogTracker.Tests.Controllers
{
    public static class Helpers
    {
        public static List<ApplicationUser> GetUsers()
        {
            var users = new List<ApplicationUser>() {
                new ApplicationUser() { Id = "ADM", UserName = "admin@toptal.com" },
                new ApplicationUser() { Id = "MGR", UserName = "manager@toptal.com" },
                new ApplicationUser() { Id = "USR", UserName = "user@toptal.com" }
            };

            return users;
        }

        public static List<Jog> GetJogs()
        {
            var jogs = new List<Jog>();
            jogs.Add(new Jog { JogId = 1, Date = DateTime.Parse("3/3/2018"), UserId = "USR", Distance = 6.5M, Duration = 71 });
            jogs.Add(new Jog { JogId = 2, Date = DateTime.Parse("3/2/2018"), UserId = "USR", Distance = 7.3M, Duration = 68 });
            jogs.Add(new Jog { JogId = 3, Date = DateTime.Parse("3/1/2018"), UserId = "USR", Distance = 6.2M, Duration = 80 });
            jogs.Add(new Jog { JogId = 4, Date = DateTime.Parse("3/3/2018"), UserId = "ADM", Distance = 8.1M, Duration = 75 });
            jogs.Add(new Jog { JogId = 5, Date = DateTime.Parse("3/2/2018"), UserId = "ADM", Distance = 7.7M, Duration = 66 });
            jogs.Add(new Jog { JogId = 6, Date = DateTime.Parse("2/27/2018"), UserId = "MGR", Distance = 5.9M, Duration = 52 });

            foreach (var jog in jogs)
            {
                var users = GetUsers();
                jog.User = users.SingleOrDefault(u => u.Id == jog.UserId);
            }
            return jogs;
        }

        public static Mock<IPrincipal> GetAnonymousPrincipal()
        {
            var mockPrincipal = new Mock<IPrincipal>();
            mockPrincipal.Setup(m => m.Identity.IsAuthenticated).Returns(false);

            return mockPrincipal;
        }

        public static Mock<IPrincipal> GetAdminPrincipal()
        {
            return GetAuthenticatedPrincipal("admin@toptal.com", "Admin");
        }

        public static Mock<IPrincipal> GetUserManagerPrincipal()
        {
            return GetAuthenticatedPrincipal("manager@toptal.com", "User Manager");
        }

        public static Mock<IPrincipal> GetUserPrincipal()
        {
            return GetAuthenticatedPrincipal("user@toptal.com", "User");
        }

        private static Mock<IPrincipal> GetAuthenticatedPrincipal(string name, string role)
        {
            var mockPrincipal = new Mock<IPrincipal>();
            mockPrincipal.Setup(m => m.Identity.IsAuthenticated).Returns(true);
            mockPrincipal.Setup(m => m.Identity.Name).Returns(name);
            mockPrincipal.Setup(m => m.IsInRole(role)).Returns(true);

            return mockPrincipal;
        }
    }
}
