﻿using JogTracker.Controllers;
using JogTracker.DAL;
using JogTracker.Models;
using JogTracker.Tests.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Results;

namespace JogTracker.Tests.Controllers
{
    [TestClass]
    public class AccountControllerTest
    {
        private Mock<ApplicationUserManager> _mockUserManager;
        private Mock<ISecureDataFormat<AuthenticationTicket>> _mockAccessTokenFormat;

        public AccountControllerTest()
        {
            var mockUserStore = new Mock<IUserStore<ApplicationUser>>();
            _mockUserManager = new Mock<ApplicationUserManager>(mockUserStore.Object, null, null, null, null, null, null, null, null);
            _mockAccessTokenFormat = new Mock<ISecureDataFormat<AuthenticationTicket>>();
        }

        [TestMethod]
        public async Task TestRegister()
        {
            // Arrange
            var model = new RegisterBindingModel
            {
                Email = "user@email.com",
                Password = "Abc123!",
                ConfirmPassword = "Abc123!"
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetAnonymousPrincipal();

            var controller = new AccountController(_mockUserManager.Object, _mockAccessTokenFormat.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Register(model);
            var result = actionResult as OkNegotiatedContentResult<UserInfoViewModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
        }

    }
}
