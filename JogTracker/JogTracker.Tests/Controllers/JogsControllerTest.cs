﻿using JogTracker.Controllers;
using JogTracker.DAL;
using JogTracker.Models;
using JogTracker.Tests.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Results;

namespace JogTracker.Tests.Controllers
{
    [TestClass]
    public class JogsControllerTest
    {
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private IRepository<ApplicationUser> _mockUserRepository;
        private IRepository<Jog> _mockJogRepository;

        public JogsControllerTest()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _mockUserRepository = new TestUserRepository(Helpers.GetUsers());
            _mockJogRepository = new TestJogRepository(Helpers.GetJogs());

            _mockUnitOfWork.Setup(m => m.Users).Returns(_mockUserRepository);
            _mockUnitOfWork.Setup(m => m.Jogs).Returns(_mockJogRepository);
        }

        [TestMethod]
        public async Task TestGetJogSelf()
        {
            // Arrange
            var username = "user@toptal.com";
            var jogId = Helpers.GetJogs()
                .First(j => j.User.UserName == username).JogId;

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(jogId);
            var result = actionResult as OkNegotiatedContentResult<JogViewModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(jogId, result.Content.JogId);
        }

        [TestMethod]
        public async Task TestGetJogForOtherUser()
        {
            // Arrange
            var username = "user@toptal.com";
            var jogId = Helpers.GetJogs()
                .First(j => j.User.UserName == username).JogId;

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserManagerPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(jogId);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public async Task TestGetJogNotFound()
        {
            // Arrange
            var jogId = 0;

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserManagerPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(jogId);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task TestGetJogAdminForOtherUser()
        {
            // Arrange
            var username = "user@toptal.com";
            var jogId = Helpers.GetJogs()
                .First(j => j.User.UserName == username).JogId;

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(jogId);
            var result = actionResult as OkNegotiatedContentResult<JogViewModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(jogId, result.Content.JogId);
        }

        [TestMethod]
        public async Task TestGetJogsAnonymous()
        {
            // Arrange
            var username = "user@toptal.com";

            Mock<IPrincipal> mockPrincipal = Helpers.GetAnonymousPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(username);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public async Task TestGetJogsAll()
        {
            // Arrange
            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public async Task TestGetJogsSelf()
        {
            // Arrange
            var username = "user@toptal.com";
            var count = Helpers.GetJogs()
                .Where(j => j.User.UserName == username).Count();

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(username);
            var result = actionResult as OkNegotiatedContentResult<IEnumerable<JogViewModel>>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(count, result.Content.Count());
        }

        [TestMethod]
        public async Task TestGetJogsForOtherUser()
        {
            // Arrange
            var username = "user@toptal.com";

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserManagerPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(username);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public async Task TestGetJogsException()
        {
            // Arrange
            var username = "user@toptal.com";

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();
            _mockUnitOfWork.Setup(m => m.Jogs
                .FilterByAsync(It.IsAny<Expression<Func<Jog, bool>>>()))
                .ThrowsAsync(new Exception("Error"));

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(username);
            var result = actionResult as ExceptionResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Exception);
            Assert.AreEqual("Error", result.Exception.Message);
        }

        [TestMethod]
        public async Task TestGetJogsAdminAll()
        {
            // Arrange
            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();
            var count = Helpers.GetJogs().Count;

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get();
            var result = actionResult as OkNegotiatedContentResult<IEnumerable<JogViewModel>>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.IsInstanceOfType(result.Content, typeof(IEnumerable<JogViewModel>));
            Assert.AreEqual(count, result.Content.Count());
        }

        [TestMethod]
        public async Task TestGetJogsAdminForOtherUser()
        {
            // Arrange
            var username = "user@toptal.com";
            var count = Helpers.GetJogs()
                .Where(j => j.User.UserName == username).Count();

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(username);
            var result = actionResult as OkNegotiatedContentResult<IEnumerable<JogViewModel>>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(count, result.Content.Count());
        }

        [TestMethod]
        public async Task TestGetJogsAdminInvalidUser()
        {
            // Arrange
            var username = "user1@toptal.com";

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Get(username);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task TestAddJogSelf()
        {
            // Arrange
            var username = "user@toptal.com";
            var jog = new NewJogBindingModel
            {
                Username = username,
                Date = DateTime.Now.Date,
                Distance = 9.3M,
                Duration = 84
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Post(jog);
            var result = actionResult as CreatedNegotiatedContentResult<JogViewModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Location);
            Assert.IsNotNull(result.Content);
        }

        [TestMethod]
        public async Task TestAddJogExistingDate()
        {
            // Arrange
            var username = "user@toptal.com";
            var jog = new NewJogBindingModel
            {
                Username = username,
                Date = DateTime.Parse("3/3/2018"),
                Distance = 6.8M,
                Duration = 45
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Post(jog);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(ConflictResult));
        }

        [TestMethod]
        public async Task TestAddJogInvalidModel()
        {
            // Arrange
            var username = "user@toptal.com";
            var jog = new NewJogBindingModel
            {
                Username = username,
                Date = DateTime.Parse("2/1/2018"),
                Distance = 6.1M,
                Duration = 0
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;
            controller.ModelState.AddModelError("Duration", "Invalid");

            // Act
            IHttpActionResult actionResult = await controller.Post(jog);
            var result = actionResult as InvalidModelStateResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ModelState);
            Assert.IsTrue(result.ModelState.Count > 0);
            Assert.AreEqual("Duration", result.ModelState.Keys.First());
        }

        [TestMethod]
        public async Task TestAddJogException()
        {
            // Arrange
            var username = "user@toptal.com";
            var jog = new NewJogBindingModel
            {
                Username = username,
                Date = DateTime.Parse("2/1/2018"),
                Distance = 6.1M,
                Duration = 0
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).ThrowsAsync(new Exception("Error"));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Post(jog);
            var result = actionResult as ExceptionResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Exception);
            Assert.AreEqual("Error", result.Exception.Message);
        }

        [TestMethod]
        public async Task TestAddJogForOtherUser()
        {
            // Arrange
            var username = "user@toptal.com";
            var jog = new NewJogBindingModel
            {
                Username = username,
                Date = DateTime.Parse("2/25/2018"),
                Distance = 6.1M,
                Duration = 70
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserManagerPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Post(jog);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public async Task TestAddJogAdminForOtherUser()
        {
            // Arrange
            var username = "user@toptal.com";
            var jog = new NewJogBindingModel
            {
                Username = username,
                Date = DateTime.Parse("2/25/2018"),
                Distance = 8.8M,
                Duration = 74
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Post(jog);
            var result = actionResult as CreatedNegotiatedContentResult<JogViewModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Location);
            Assert.IsNotNull(result.Content);
        }

        [TestMethod]
        public async Task TestAddJogAdminInvalidUser()
        {
            // Arrange
            var username = "user1@toptal.com";
            var jog = new NewJogBindingModel
            {
                Username = username,
                Date = DateTime.Parse("2/25/2018"),
                Distance = 8.8M,
                Duration = 74
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Post(jog);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task TestUpdateJogSelf()
        {
            // Arrange
            var jog = Helpers.GetJogs().Where(j => j.UserId == "USR").First();

            var model = new JogEditBindingModel
            {
                Date = jog.Date.Date,
                Distance = jog.Distance + 1.5M,
                Duration = jog.Duration + 10
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Put(jog.JogId, model);
            var result = actionResult as OkNegotiatedContentResult<JogViewModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(jog.Distance + 1.5M, result.Content.Distance);
            Assert.AreEqual(jog.Duration + 10, result.Content.Duration);
        }

        [TestMethod]
        public async Task TestUpdateJogExistingDate()
        {
            // Arrange
            var jog = Helpers.GetJogs().Where(j => j.UserId == "USR").First();

            var model = new JogEditBindingModel
            {
                Date = DateTime.Parse("3/2/2018"),
                Distance = 9.3M,
                Duration = 84
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Put(jog.JogId, model);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(ConflictResult));
        }

        [TestMethod]
        public async Task TestUpdateJogInvalidModel()
        {
            // Arrange
            var jog = Helpers.GetJogs().Where(j => j.UserId == "USR").First();

            var model = new JogEditBindingModel
            {
                Date = jog.Date,
                Distance = jog.Distance + 2,
                Duration = 0
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;
            controller.ModelState.AddModelError("Duration", "Invalid.");

            // Act
            IHttpActionResult actionResult = await controller.Put(jog.JogId, model);
            var result = actionResult as InvalidModelStateResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ModelState);
            Assert.AreEqual("Duration", result.ModelState.Keys.First());
        }

        [TestMethod]
        public async Task TestUpdateJogException()
        {
            // Arrange
            var jog = Helpers.GetJogs().Where(j => j.UserId == "USR").First();

            var model = new JogEditBindingModel
            {
                Date = jog.Date,
                Distance = jog.Distance + 2,
                Duration = 0
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).ThrowsAsync(new Exception("Error"));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Put(jog.JogId, model);
            var result = actionResult as ExceptionResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Exception);
            Assert.AreEqual("Error", result.Exception.Message);
        }

        [TestMethod]
        public async Task TestUpdateJogForOtherUser()
        {
            // Arrange
            var jog = Helpers.GetJogs().Where(j => j.UserId == "USR").First();

            var model = new JogEditBindingModel
            {
                Date = jog.Date,
                Distance = jog.Distance + 2,
                Duration = jog.Duration + 10
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserManagerPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Put(jog.JogId, model);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public async Task TestUpdateJogNotFound()
        {
            // Arrange
            var jog = Helpers.GetJogs().Where(j => j.UserId == "USR").First();

            var model = new JogEditBindingModel
            {
                Date = jog.Date.Date,
                Distance = jog.Distance + 1.5M,
                Duration = jog.Duration + 10
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);

            // Act
            IHttpActionResult actionResult = await controller.Put(0, model);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }


        [TestMethod]
        public async Task TestUpdateJogAdminForOtherUser()
        {
            // Arrange
            var jog = Helpers.GetJogs().Where(j => j.UserId == "USR").First();

            var model = new JogEditBindingModel
            {
                Date = jog.Date.Date,
                Distance = jog.Distance + 1.5M,
                Duration = jog.Duration + 10
            };

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Put(jog.JogId, model);
            var result = actionResult as OkNegotiatedContentResult<JogViewModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Content);
            Assert.AreEqual(jog.Distance + 1.5M, result.Content.Distance);
            Assert.AreEqual(jog.Duration + 10, result.Content.Duration);
        }

        [TestMethod]
        public async Task TestDeleteJogSelf()
        {
            // Arrange
            var jogId = Helpers.GetJogs().First(j => j.UserId == "USR").JogId;

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Delete(jogId);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public async Task TestDeleteException()
        {
            // Arrange
            var jogId = Helpers.GetJogs().First(j => j.UserId == "USR").JogId;

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).ThrowsAsync(new Exception("Error"));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Delete(jogId);
            var result = actionResult as ExceptionResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Exception);
            Assert.AreEqual("Error", result.Exception.Message);
        }

        [TestMethod]
        public async Task TestDeleteJogForOtherUser()
        {
            // Arrange
            var jogId = Helpers.GetJogs().First(j => j.UserId == "USR").JogId;

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserManagerPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Delete(jogId);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public async Task TestDeleteJogNotFound()
        {
            // Arrange
            var jogId = 0;

            Mock<IPrincipal> mockPrincipal = Helpers.GetUserPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Delete(jogId);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task TestDeleteJogAdminForOtherUser()
        {
            // Arrange
            var jogId = Helpers.GetJogs().First(j => j.UserId == "USR").JogId;

            Mock<IPrincipal> mockPrincipal = Helpers.GetAdminPrincipal();

            _mockUnitOfWork.Setup(m => m.SaveAsync()).Returns(Task.FromResult(true));
            var controller = new JogsController(_mockUnitOfWork.Object);
            controller.User = mockPrincipal.Object;

            // Act
            IHttpActionResult actionResult = await controller.Delete(jogId);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }
    }
}
