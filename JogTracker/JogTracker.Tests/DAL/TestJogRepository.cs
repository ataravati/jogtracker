﻿using JogTracker.DAL;
using JogTracker.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JogTracker.Tests.DAL
{
    class TestJogRepository : IRepository<Jog>
    {
        ObservableCollection<Jog> _data;

        public TestJogRepository()
        {
            _data = new ObservableCollection<Jog>();
        }

        public TestJogRepository(IEnumerable<Jog> data)
        {
            _data = new ObservableCollection<Jog>(data);
        }

        public IQueryable<Jog> All()
        {
            return _data.AsQueryable();
        }

        public async Task<List<Jog>> AllAsync()
        {
            return await Task.FromResult(this.All().ToList());
        }

        public IQueryable<Jog> FilterBy(Expression<Func<Jog, bool>> predicate)
        {
            return _data.AsQueryable().Where(predicate);
        }

        public async Task<List<Jog>> FilterByAsync(Expression<Func<Jog, bool>> predicate)
        {
            return await Task.FromResult(this.FilterBy(predicate).ToList());
        }

        public Jog Find(params object[] id)
        {
            return _data.SingleOrDefault(j => j.JogId == (int)id.Single());
        }

        public Task<Jog> FindAsync(params object[] id)
        {
            return Task.FromResult(_data.SingleOrDefault(j => j.JogId == (int)id.Single()));
        }

        public void Add(Jog jog)
        {
            _data.Add(jog);
        }


        public void Delete(Jog jog)
        {
            _data.Remove(jog);
        }
    }
}
