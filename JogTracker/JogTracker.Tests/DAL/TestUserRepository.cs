﻿using JogTracker.DAL;
using JogTracker.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JogTracker.Tests.DAL
{
    class TestUserRepository : IRepository<ApplicationUser>
    {
        ObservableCollection<ApplicationUser> _data;

        public TestUserRepository()
        {
            _data = new ObservableCollection<ApplicationUser>();
        }

        public TestUserRepository(IEnumerable<ApplicationUser> data)
        {
            _data = new ObservableCollection<ApplicationUser>(data);
        }

        public IQueryable<ApplicationUser> All()
        {
            return _data.AsQueryable();
        }

        public async Task<List<ApplicationUser>> AllAsync()
        {
            return await Task.FromResult(this.All().ToList());
        }

        public IQueryable<ApplicationUser> FilterBy(Expression<Func<ApplicationUser, bool>> predicate)
        {
            return _data.AsQueryable().Where(predicate);
        }

        public async Task<List<ApplicationUser>> FilterByAsync(Expression<Func<ApplicationUser, bool>> predicate)
        {
            return await Task.FromResult(this.FilterBy(predicate).ToList());
        }

        public ApplicationUser Find(params object[] id)
        {
            return _data.SingleOrDefault(u => u.Id == (string)id.Single());
        }

        public Task<ApplicationUser> FindAsync(params object[] id)
        {
            return Task.FromResult(_data.SingleOrDefault(u => u.Id == (string)id.Single()));
        }

        public void Add(ApplicationUser user)
        {
            _data.Add(user);
        }


        public void Delete(ApplicationUser user)
        {
            _data.Remove(user);
        }
    }
}
