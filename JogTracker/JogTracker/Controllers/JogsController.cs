﻿using JogTracker.DAL;
using JogTracker.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace JogTracker.Controllers
{
    [Authorize]
    [RoutePrefix("api/Jogs")]
    public class JogsController : ApiController
    {
        private IUnitOfWork _unitOfWork;

        public JogsController()
        {
            _unitOfWork = new UnitOfWork();
        }

        public JogsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET /api/Jogs/

        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                var jog = await _unitOfWork.Jogs.FindAsync(id);

                if (jog == null)
                {
                    return NotFound();
                }

                // Only Admin is allowed to get other users' data...
                if (!User.IsInRole("Admin") && jog.User.UserName != User.Identity.Name)
                {
                    return Unauthorized();
                }

                return Ok(jog.ToViewModel());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        // GET /api/Jogs/

        public async Task<IHttpActionResult> Get([FromUri]string username = null)
        {
            // Only Admin is allowed to get other users' data...
            if (!User.IsInRole("Admin") && (string.IsNullOrEmpty(username) || username != User.Identity.Name))
            {
                return Unauthorized();
            }

            try
            {
                string userId = "";
                if (!string.IsNullOrEmpty(username))
                { 
                    var user = _unitOfWork.Users
                        .FilterBy(u => u.UserName == username).SingleOrDefault();

                    if (user == null)
                    {
                        return NotFound();
                    }

                    userId = user.Id;
                }

                var jogs = await _unitOfWork.Jogs
                    .FilterByAsync(j => string.IsNullOrEmpty(userId) 
                        || j.UserId == userId);
                    
                var response = jogs.Select(j => j.ToViewModel());

                return Ok(response);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST /api/Jogs/

        [HttpPost]
        public async Task<IHttpActionResult> Post(NewJogBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                // Return a bad request response if the model state is invalid...
                return BadRequest(ModelState);
            }

            // Only Admin is allowed to insert data for other users...
            if (!User.IsInRole("Admin") && model.Username != User.Identity.Name)
            {
                return Unauthorized();
            }

            try
            {
                var user = _unitOfWork.Users
                    .FilterBy(u => u.UserName == model.Username).SingleOrDefault();
                if(user == null)
                {
                    return NotFound();
                }

                // There can only been one entry per date/user.
                var jogs = await _unitOfWork.Jogs
                    .FilterByAsync(j => j.User.UserName == model.Username && j.Date == model.Date.Date);
                if (jogs.Any())
                {
                    return Conflict();
                }

                var jog = new Jog
                {
                    UserId = user.Id,
                    User = user,
                    Date = model.Date.Date,
                    Distance = model.Distance,
                    Duration = model.Duration
                };

                _unitOfWork.Jogs.Add(jog);
                await _unitOfWork.SaveAsync();

                var location = string.Format("/api/Jogs/{0}", jog.JogId);

                return Created<JogViewModel>(location, jog.ToViewModel());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // PUT /api/Jogs/5

        [HttpPut]
        public async Task<IHttpActionResult> Put(int id, [FromBody]JogEditBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                // Return a bad request response if the model state is invalid...
                return BadRequest(ModelState);
            }

            try
            {
                var jog = await _unitOfWork.Jogs.FindAsync(id);

                if (jog == null)
                { 
                    return NotFound();
                }
                
                // Only Admin is allowed to update other users' data...
                if (!User.IsInRole("Admin") && jog.User.UserName != User.Identity.Name)
                {
                    return Unauthorized();
                }

                // There can only been one entry per date/user.
                if (model.Date != jog.Date)
                {
                    var jogs = await _unitOfWork.Jogs
                        .FilterByAsync(j => j.User.UserName == jog.User.UserName && j.Date == model.Date.Date);
                    if (jogs.Any())
                    {
                        return Conflict();
                    }
                }

                jog.Date = model.Date.Date;
                jog.Distance = model.Distance;
                jog.Duration = model.Duration;

                await _unitOfWork.SaveAsync();

                return Ok(jog.ToViewModel());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // DELETE /api/Jogs/5

        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                var jog = await _unitOfWork.Jogs.FindAsync(id);

                if (jog == null)
                {
                    return NotFound();
                }

                // Only Admin is allowed to delete other users' data...
                if (!User.IsInRole("Admin") && jog.User.UserName != User.Identity.Name)
                {
                    return Unauthorized();
                }
 
                _unitOfWork.Jogs.Delete(jog);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return Ok();
        }
    }
}
