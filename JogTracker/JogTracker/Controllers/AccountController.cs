﻿using JogTracker.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace JogTracker.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [HttpPost]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            var result = await RegisterUser(model, "User");

            return result;
        }

        // GET api/Account/GetUsers
        [Authorize(Roles = "Admin, User Manager")]
        [Route("GetUsers")]
        public async Task<IHttpActionResult> GetUsers()
        {
            var users = await UserManager.Users.ToListAsync();

            var response = users.Select(u => u.ToViewModel(UserManager));

            return Ok(response);
        }

        [Authorize(Roles = "Admin, User Manager")]
        [HttpPost]
        [Route("AddUser")]
        public async Task<IHttpActionResult> AddUser(RegisterBindingModel model, [FromUri]string role = null)
        {
            if(string.IsNullOrEmpty(role))
            {
                role = "User";
            }

            if (!User.IsInRole("Admin") && role == "Admin")
            {
                return Unauthorized();
            }

            var result = await RegisterUser(model, role);

            return result;
        }

        [Authorize(Roles = "Admin, User Manager")]
        [HttpPut]
        [Route("UpdateUser/{id}")]
        public async Task<IHttpActionResult> UpdateUser([FromUri]string id, UserEditBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByIdAsync(id);
            if(user == null)
            {
                return NotFound();
            }

            if (!User.IsInRole("Admin") && model.Role == "Admin")
            {
                return Unauthorized();
            }

            user.Email = model.Email;
            user.UserName = model.Email;

            // Start a transaction...
            var dbContext = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                IdentityResult result = await UserManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    var roles = await UserManager.GetRolesAsync(user.Id);
                    result = await UserManager.RemoveFromRolesAsync(user.Id, roles.ToArray());

                    if (result.Succeeded)
                    {
                        result = await UserManager.AddToRoleAsync(user.Id, model.Role);

                        if (result.Succeeded)
                        { 
                            // Commit the transaction...
                            transaction.Commit();

                            return Ok(user.ToViewModel(UserManager));
                        }   
                    }
                }

                // Rollback the transaction...
                transaction.Rollback();

                return GetErrorResult(result);
            }
        }

        [Authorize(Roles = "Admin, User Manager")]
        [HttpDelete]
        [Route("DeleteUser/{id}")]
        public async Task<IHttpActionResult> DeleteUser(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if(user == null)
            {
                return NotFound();
            }

            var roleId = user.Roles.Select(r => r.RoleId).FirstOrDefault();
            if(!User.IsInRole("Admin") && roleId == "admin")
            {
                return Unauthorized();
            }

            IdentityResult result = await UserManager.DeleteAsync(user);

            if(result.Succeeded)
            {
                return Ok();
            }

            return GetErrorResult(result);
        }

        // POST api/Account/SetPassword
        [Authorize(Roles = "Admin, User Manager")]
        [Route("SetPassword/{id}")]
        public async Task<IHttpActionResult> SetPassword([FromUri]string id, SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var roleId = user.Roles.Select(r => r.RoleId).FirstOrDefault();
            if (!User.IsInRole("Admin") && roleId == "admin")
            {
                return Unauthorized();
            }

            // Start a transaction...
            var dbContext = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                IdentityResult result = await UserManager.RemovePasswordAsync(user.Id);
                if (result.Succeeded)
                {
                    result = await UserManager.AddPasswordAsync(user.Id, model.NewPassword);

                    if (result.Succeeded)
                    {
                        // Commit the transaction...
                        transaction.Commit();

                        return Ok();
                    }
                }

                // Rollback the transaction...
                transaction.Rollback();

                return GetErrorResult(result);
            }
        }

        // GET api/Account/GetUserRole
        [Route("GetUserRole")]
        public async Task<IHttpActionResult> GetUserRole()
        {
            var roles = await UserManager.GetRolesAsync(User.Identity.GetUserId());
            var role = roles.FirstOrDefault();

            return Ok(role);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private async Task<IHttpActionResult> RegisterUser(RegisterBindingModel model, string role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

            // Start a transaction...
            var dbContext = HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                IdentityResult result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    result = await UserManager.AddToRoleAsync(user.Id, role);

                    if (result.Succeeded)
                    {
                        // Commit the transaction...
                        transaction.Commit();

                        return Ok(user.ToViewModel(UserManager));
                    }
                }

                // Rollback the transaction...
                transaction.Rollback();

                return GetErrorResult(result);
            }
        }

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        #endregion
    }
}
