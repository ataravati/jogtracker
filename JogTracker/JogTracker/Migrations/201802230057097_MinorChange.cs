namespace JogTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MinorChange : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Jog", "LastModifiedDate");
            DropColumn("dbo.Jog", "ModifiedBy");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Jog", "ModifiedBy", c => c.String());
            AddColumn("dbo.Jog", "LastModifiedDate", c => c.DateTime(nullable: false));
        }
    }
}
