namespace JogTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateType : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Jog", "IX_User_Date");
            AlterColumn("dbo.Jog", "Date", c => c.DateTime(nullable: false, storeType: "date"));
            CreateIndex("dbo.Jog", new[] { "UserId", "Date" }, unique: true, name: "IX_User_Date");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Jog", "IX_User_Date");
            AlterColumn("dbo.Jog", "Date", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Jog", new[] { "UserId", "Date" }, unique: true, name: "IX_User_Date");
        }
    }
}
