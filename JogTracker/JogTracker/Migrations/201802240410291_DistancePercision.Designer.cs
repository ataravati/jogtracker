// <auto-generated />
namespace JogTracker.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DistancePercision : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DistancePercision));
        
        string IMigrationMetadata.Id
        {
            get { return "201802240410291_DistancePercision"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
