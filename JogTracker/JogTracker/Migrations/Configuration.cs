namespace JogTracker.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<JogTracker.Models.JogTrackerDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(JogTracker.Models.JogTrackerDB context)
        {
            // Add the roles to the database, if they're not added...
            context.Roles.AddOrUpdate(r => r.Id,
                new IdentityRole { Id = "admin", Name = "Admin" },
                new IdentityRole { Id = "manager", Name = "User Manager" },
                new IdentityRole { Id = "user", Name = "User" });
        }
    }
}
