namespace JogTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DistancePercision : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Jog", "Distance", c => c.Decimal(nullable: false, precision: 3, scale: 1));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Jog", "Distance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
