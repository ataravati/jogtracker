﻿using JogTracker.Models;
using System;
using System.Threading.Tasks;

namespace JogTracker.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Jog> Jogs { get; }
        IRepository<ApplicationUser> Users { get; }

        void Save();
        Task SaveAsync();
    }
}
 