﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JogTracker.DAL
{
    public interface IRepository<T> where T : class 
    {
        IQueryable<T> All();
        Task<List<T>> AllAsync();
        IQueryable<T> FilterBy(Expression<Func<T, bool>> predicate);
        Task<List<T>> FilterByAsync(Expression<Func<T, bool>> predicate);
        T Find(params object[] id);
        Task<T> FindAsync(params object[] id);
        void Add(T t);
        void Delete(T t);
    }
}