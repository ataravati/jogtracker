﻿using JogTracker.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JogTracker.DAL
{
    public class UnitOfWork : IUnitOfWork, IDisposable 
    {
        protected bool _disposed;
        protected JogTrackerDB _context;

        private IRepository<Jog> _jogs;
        private IRepository<ApplicationUser> _users;

        public UnitOfWork()
        {
            _context = new JogTrackerDB();
            _context.Configuration.LazyLoadingEnabled = false;
        }

        public IRepository<Jog> Jogs
        {
            get
            {
                if (_jogs == null)
                    _jogs = new JogRepository(_context);

                return _jogs;
            }
        }

        public IRepository<ApplicationUser> Users
        {
            get
            {
                if (_users == null)
                    _users = new UserRepository(_context);

                return _users;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

