﻿using JogTracker.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JogTracker.DAL
{
    public class UserRepository : IRepository<ApplicationUser>
    {
        private DbContext _context;
        private DbSet<ApplicationUser> _dbSet;        

        public UserRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<ApplicationUser>();
        }

        public IQueryable<ApplicationUser> All()
        {
            return _dbSet;
        }

        public async Task<List<ApplicationUser>> AllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public IQueryable<ApplicationUser> FilterBy(Expression<Func<ApplicationUser, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public async Task<List<ApplicationUser>> FilterByAsync(Expression<Func<ApplicationUser, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }

        public ApplicationUser Find(params object[] id)
        {
            return _dbSet.Find(id);
        }

        public async Task<ApplicationUser> FindAsync(params object[] id)
        {
            return await _dbSet.FindAsync(id);
        }

        public void Add(ApplicationUser user)
        {
            _dbSet.Add(user);
        }

        public void Delete(ApplicationUser user)
        {
            _dbSet.Remove(user);
        }
    }
}