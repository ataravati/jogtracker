﻿using JogTracker.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JogTracker.DAL
{
    public class JogRepository : IRepository<Jog>
    {
        private DbContext _context;
        private DbSet<Jog> _dbSet;        

        public JogRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<Jog>();
        }

        public IQueryable<Jog> All()
        {
            return _dbSet.Include(j => j.User);
        }

        public async Task<List<Jog>> AllAsync()
        {
            return await _dbSet.Include(j => j.User).ToListAsync();
        }

        public IQueryable<Jog> FilterBy(Expression<Func<Jog, bool>> predicate)
        {
            return _dbSet.Include(j => j.User).Where(predicate);
        }

        public async Task<List<Jog>> FilterByAsync(Expression<Func<Jog, bool>> predicate)
        {
            return await _dbSet.Include(j => j.User).Where(predicate).ToListAsync();
        }

        public Jog Find(params object[] id)
        {
            var jogId = (int)id[0];
            return this.FilterBy(j => j.JogId == jogId).SingleOrDefault();
        }

        public async Task<Jog> FindAsync(params object[] id)
        {
            var jogId = (int)id[0];
            return await this.FilterBy(j => j.JogId == jogId).SingleOrDefaultAsync();
        }

        public void Add(Jog jog)
        {
            _dbSet.Add(jog);
        }

        public void Delete(Jog jog)
        {
            _dbSet.Remove(jog);
        }
    }
}