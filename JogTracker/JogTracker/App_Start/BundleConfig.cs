﻿using System.Web;
using System.Web.Optimization;

namespace JogTracker
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/loading-bar.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                    "~/Scripts/angular.js",
                    "~/Scripts/angular-route.js",
                    "~/Scripts/angular-messages.js",
                    "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                    "~/Scripts/ngStorage.js",
                    "~/Scripts/loading-bar.js",
                    "~/Scripts/moment.js",
                    "~/Scripts/angular-moment.js",
                    "~/Scripts/angular-filter.js",
                    "~/Scripts/dirPagination.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                    "~/app/jogTrackerApp.js",
                    "~/app/services/authService.js",
                    "~/app/services/authInterceptorService.js",
                    "~/app/services/commonService.js",
                    "~/app/services/jogService.js",
                    "~/app/services/userService.js",
                    "~/app/directives/compareToDirective.js",
                    "~/app/filters/dateRangeFilter.js",
                    "~/app/filters/averageFilter.js",
                    "~/app/controllers/indexController.js",
                    "~/app/controllers/loginController.js",
                    "~/app/controllers/registerController.js",
                    "~/app/controllers/changePasswordController.js",
                    "~/app/controllers/jogsController.js",
                    "~/app/controllers/jogFormController.js",
                    "~/app/controllers/usersController.js",
                    "~/app/controllers/userFormController.js",
                    "~/app/controllers/setPasswordFormController.js",
                    "~/app/controllers/confirmDialogController.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
