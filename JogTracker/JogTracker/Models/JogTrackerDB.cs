namespace JogTracker.Models
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class JogTrackerDB : IdentityDbContext<ApplicationUser>
    {
        public JogTrackerDB()
            : base("name=DefaultConnection")
        {
            Database.SetInitializer<JogTrackerDB>(new DropCreateDatabaseIfModelChanges<JogTrackerDB>());
        }

        public virtual DbSet<Jog> Jogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Set percision for the Jog.Distance column...
            modelBuilder.Entity<Jog>().Property(p => p.Distance).HasPrecision(3, 1);

            // Prevent EF from giving plural names to the tables...
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}