﻿using Microsoft.AspNet.Identity;
using System;
using System.Globalization;
using System.Linq;

namespace JogTracker.Models
{
    public static class Mappers
    {
        public static JogViewModel ToViewModel(this Jog jog)
        {
            var viewModel = new JogViewModel
            {
                JogId = jog.JogId,
                Username = jog.User.UserName,
                WeekStartDate = jog.Date.WeekStartDate(),
                Date = jog.Date.Date,
                Distance = jog.Distance,
                Duration = jog.Duration,
                AverageSpeed = jog.AverageSpeed()
            };

            return viewModel;
        }

        public static UserInfoViewModel ToViewModel(this ApplicationUser user, ApplicationUserManager userManager)
        {
            var viewModel = new UserInfoViewModel
            {
                UserId = user.Id,
                Email = user.Email,
                Role = userManager.GetRoles(user.Id).FirstOrDefault()
            };

            return viewModel;
        }

        private static decimal AverageSpeed(this Jog jog)
        {
            return 60 * jog.Distance / jog.Duration;
        }

        private static DateTime WeekStartDate(this DateTime date)
        {
            var cultureInfo = CultureInfo.CurrentCulture;
            DayOfWeek firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            int offset = firstDayOfWeek - date.DayOfWeek;
            DateTime weekStartDate = date.AddDays(offset).Date;

            return weekStartDate;
        }
    }
}