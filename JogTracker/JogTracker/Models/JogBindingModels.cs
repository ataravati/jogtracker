﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JogTracker.Models
{
    public class NewJogBindingModel
    {
        [Required]
        [StringLength(256)]
        public string Username { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [Range(1, 50, ErrorMessage = "The {0} must be between {1} and {2} kilometers.")]
        public decimal Distance { get; set; }

        [Required]
        [Range(1, 480, ErrorMessage = "The {0} must be between {1} and {2} minutes.")]
        public int Duration { get; set; }
    }

    public class JogEditBindingModel
    {
        [Required]
        public DateTime Date { get; set; }

        [Required]
        [Range(1, 50, ErrorMessage = "The {0} must be between {1} and {2} kilometers.")]
        public decimal Distance { get; set; }

        [Required]
        [Range(1, 480, ErrorMessage = "The {0} must be between {1} and {2} minutes.")]
        public int Duration { get; set; }
    }
}