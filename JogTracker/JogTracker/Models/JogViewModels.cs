﻿using System;

namespace JogTracker.Models
{
    public class JogViewModel
    {
        public int JogId { get; set; }
        public string Username { get; set; }
        public DateTime WeekStartDate { get; set; }
        public DateTime Date { get; set; }
        public decimal Distance { get; set; }
        public int Duration { get; set; }
        public decimal AverageSpeed { get; set; } 
    }

    public class JogReportViewModel
    {
        public string Username { get; set; }
        public DateTime FirstDayOfWeek { get; set; }
        public decimal AverageDistance { get; set; }
        public decimal AverageSpeed { get; set; }
    }
}