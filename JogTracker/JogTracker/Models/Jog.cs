﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JogTracker.Models
{
    [Table("Jog")]
    public class Jog
    { 
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int JogId { get; set; }

        [Required]
        [Index("IX_User_Date", 1, IsUnique = true)]        
        public string UserId { get; set; }

        [Required]
        [Index("IX_User_Date", 2, IsUnique = true)]
        [Column(TypeName = "Date")]
        public DateTime Date { get; set; }

        [Required]        
        public decimal Distance { get; set; }

        [Required]
        public int Duration { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }        
    }
}