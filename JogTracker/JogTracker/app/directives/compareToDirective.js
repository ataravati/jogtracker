﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.directive("compareTo", function () {
        return {
            restrict: 'A',
            require: "ngModel",
            scope: {
                otherValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (val) {
                    return val === scope.otherValue;
                };

                scope.$watch("otherValue", function () {
                    ngModel.$validate();
                });
            }
        };
    });
}());