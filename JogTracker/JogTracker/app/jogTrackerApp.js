﻿(function () {
    'use strict';

    var app = angular.module("jogTrackerApp",
        ["ngRoute", "ngMessages", "ngStorage", "ui.bootstrap", "angular-loading-bar", "angularMoment", "angular.filter", "angularUtils.directives.dirPagination"]);

    app.config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');

        $routeProvider.when("/", {
            controller: "jogsController",
            templateUrl: "/app/views/jogs.html"
        })
        .when("/login", {
            controller: "loginController",
            templateUrl: "/app/views/login.html",
            anonymous: true
        })
        .when("/register", {
            controller: "registerController",
            templateUrl: "/app/views/register.html",
            anonymous: true
        })
        .when("/changePassword", {
            controller: "changePasswordController",
            templateUrl: "/app/views/changePassword.html"
        })
        .when("/jogs", {
            redirectTo: "/"
        })
        .when("/jogs/:username", {
            controller: "jogsController",
            templateUrl: "/app/views/jogs.html",
            roles: ["Admin"]
        })
        .when("/users", {
            controller: "usersController",
            templateUrl: "/app/views/users.html",
            roles: ["Admin","User Manager"]
        })
        .when("/notFound", {
            controller: "notFoundController",
            templateUrl: "/app/views/notFound.html"
        })
        .when("/unauthorized", {
            controller: "unauthorizedController",
            templateUrl: "/app/views/unauthorized.html"
        })
        .otherwise({ redirectTo: "/" });
    }]);

    app.config(["$httpProvider", function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    }]);
    
    app.constant('appSettings', {
        apiServiceBaseUri: 'http://localhost:36622/'
    });

    app.run(["$rootScope", "$location", "authService", function ($rootScope, $location, authService) {
        // Fill the authentication data (get it from session storage, if exists)... 
        authService.fillAuthData();

        // Register listener to watch route changes...
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            var currentUser = authService.currentUser;

            // Redirect to login page, if the user is not authenticated...
            if (!currentUser.isAuthenticated && !next.anonymous) {
                $location.path('/login');
                event.preventDefault();
            }
            // Redirect to home page, if the user is authorized 
            // and tries to open the anonymous pages (login and register)...
            else if (currentUser.isAuthenticated && next.anonymous) {
                $location.path('/');
                event.preventDefault();
            }
            // Redirect to home page, if the next route requires certain roles 
            // and the user does not have any of those roles...
            else if (next.roles && next.roles.length > 0) {
                if (!currentUser.role || next.roles.indexOf(currentUser.role) === -1) {
                    $location.path('/');
                    event.preventDefault();
                }
            }
        });
    }]);
}()); 