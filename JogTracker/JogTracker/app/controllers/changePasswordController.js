﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('changePasswordController',
        ["$scope", "$location", "$timeout", "commonService", "userService",
        function ($scope, $location, $timeout, commonService, userService)
    {
        $scope.success = false;
        $scope.errors = null;
        $scope.data = {
            oldPassword: "",
            newPassword: "",
            confirmPassword: ""
        };

        $scope.submit = function (formValid) {
            $scope.errors = null;
            $scope.success = false;

            if (formValid) {
                userService.changePassword($scope.data).then(function (response) {
                    $scope.success = true;

                    // Redirect to home page in 2 seconds...
                    var timer = $timeout(function () {
                        $timeout.cancel(timer);
                        $location.path('/');
                    }, 2000);
                }, function (response) {
                    $scope.errors = commonService.handleError(response);
                });
            }
        };
    }]);
}());