﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('userFormController',
        ["$scope", "$uibModalInstance", "commonService", "user", "userService", "authService",
        function ($scope, $uibModalInstance, commonService, user, userService, authService)
    {
        $scope.isNew = !("userId" in user);
        $scope.userData = angular.copy(user);
        $scope.errors = null;
        $scope.currentUser = authService.currentUser;
        $scope.roles = ["User", "User Manager"];
        if ($scope.currentUser.role === 'Admin') {
            $scope.roles.push('Admin');
        }

        $scope.submit = function (formValid) {
            $scope.errors = null;

            if (formValid) {
                if ($scope.isNew) {
                    // Add new user...
                    userService.addUser($scope.userData).then(function (response) {
                        $uibModalInstance.close(response.data);
                    }, function (response) {
                        $scope.errors = commonService.handleError(response);
                    });
                }
                else {
                    // Update user...
                    userService.updateUser($scope.userData).then(function (response) {
                        $uibModalInstance.close(response.data);
                    }, function (response) {
                        $scope.errors = commonService.handleError(response);
                    });
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }]);
}());