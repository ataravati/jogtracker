﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('indexController', ["$scope", "$location", "authService",
        function ($scope, $location, authService) {
            $scope.logout = function () {
                authService.logout();
                $location.path('/login');
            };

            $scope.isCollapsed = true;
            $scope.currentUser = authService.currentUser;

            $scope.showUsersMenu = function () {
                if (authService.currentUser.role === 'Admin' || authService.currentUser.role === 'User Manager')
                    return true;
                
                return false;
            };
    }]);
}());