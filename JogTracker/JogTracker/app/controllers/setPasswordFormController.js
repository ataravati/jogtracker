﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('setPasswordFormController',
        ["$scope", "$uibModalInstance", "commonService", "$timeout", "userId", "userService",
        function ($scope, $uibModalInstance, commonService, $timeout, userId, userService)
    {
            $scope.success = false;
            $scope.errors = null;
            $scope.data = { userId: userId };

            $scope.submit = function (formValid) {
            $scope.errors = null;

            if (formValid) {
                userService.setPassword($scope.data).then(function (response) {
                    $scope.success = true;

                    // Close the form in a second...
                    var timer = $timeout(function () {
                        $timeout.cancel(timer);
                        $uibModalInstance.close();
                    }, 1000);
                }, function (response) {
                    $scope.errors = commonService.handleError(response);
                });
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }]);
}());