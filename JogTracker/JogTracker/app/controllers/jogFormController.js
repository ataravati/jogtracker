﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('jogFormController',
        ["$scope", "$uibModalInstance", "moment", "commonService", "jog", "jogService",
        function ($scope, $uibModalInstance, moment, commonService, jog, jogService)
    {
        $scope.isNew = !("jogId" in jog);
        $scope.jogData = angular.copy(jog);
        if ($scope.jogData)
            $scope.jogData.date = moment($scope.jogData.date, 'YYYY-MM-DD').toDate();
        $scope.errors = null;

        // Datepicker
        $scope.opened = false;
        $scope.datepickerOptions = {
            showWeeks: false,
            maxDate: new Date()
        };        
        $scope.openDatePicker = function () {
            $scope.opened = true;
        };

        $scope.submit = function (formValid) {
            $scope.errors = null;

            if (formValid) {
                var jogData = angular.copy($scope.jogData);
                jogData.date = moment(jogData.date).format('YYYY-MM-DD');

                if ($scope.isNew) {
                    // Add new jog...
                    jogService.addJog(jogData).then(function (response) {
                        $uibModalInstance.close(response.data);
                    }, function (response) {
                        $scope.errors = commonService.handleError(response);
                    });
                }
                else {
                    // Update jog...
                    jogService.updateJog(jogData).then(function (response) {
                        $uibModalInstance.close(response.data);
                    }, function (response) {
                        $scope.errors = commonService.handleError(response);
                    });
                }
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        };
    }]);
}());