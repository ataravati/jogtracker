﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('loginController', ["$scope", "$location", "authService", function ($scope, $location, authService)
    {
        $scope.error = "";

        $scope.loginInfo = {
            username: "",
            password: ""
        };

        $scope.login = function (formValid) {
            $scope.error = "";

            if (formValid) {
                authService.login($scope.loginInfo).then(function (response) {
                    $location.path('/');
                },
                function (response) {
                    $scope.error = response.error_description;
                });
            }
        };
    }]);
}());