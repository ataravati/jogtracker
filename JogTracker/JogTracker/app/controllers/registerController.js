﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('registerController',
        ["$scope", "$location", "$timeout", "commonService", "authService",
        function ($scope, $location, $timeout, commonService, authService)
    {
        $scope.success = false;
        $scope.errors = null;
        $scope.userInfo = {
            email: "",
            password: "",
            confirmPassword: ""
        };

        $scope.register = function (formValid) {
            $scope.errors = null;
            $scope.success = false;

            if (formValid) {
                authService.register($scope.userInfo).then(function (response) {
                    $scope.success = true;

                    // Redirect to the login page in 2 seconds...
                    var timer = $timeout(function () {
                        $timeout.cancel(timer);
                        $location.path('/login');
                    }, 2000);
                }, function (response) {
                    $scope.errors = commonService.handleError(response);
                });
            }
        };
    }]);
}());