﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('jogsController',
        ["$scope", "$routeParams", "$uibModal", "cfpLoadingBar", "commonService", "authService", "jogService",
        function ($scope, $routeParams, $uibModal, cfpLoadingBar, commonService, authService, jogService)
    {
        $scope.error = "";
        $scope.username = $routeParams.username ? $routeParams.username : authService.currentUser.username;
        $scope.jogs = [];

        // Sort
        $scope.sortKey = 'date';
        $scope.reverse = true;
        $scope.sort = function (key) {
            if ($scope.sortKey === key) {
                $scope.reverse = !$scope.reverse;
            }
            else {
                $scope.reverse = false;
            }
            $scope.sortKey = key;
        };

        // Filter datepickers
        $scope.filterFrom = null;
        $scope.filterTo = null;
        $scope.fromOpened = false;
        $scope.toOpened = false;
        $scope.fromOptions = {
            showWeeks: false,
            maxDate: new Date()
        };
        $scope.toOptions = {
            showWeeks: false,
            minDate: $scope.filterFrom,
            maxDate: new Date()
        };
        $scope.openFrom = function () {
            $scope.fromOptions.maxDate = $scope.filterTo || new Date();
            $scope.fromOpened = true;
        };
        $scope.openTo = function () {
            $scope.toOptions.minDate = $scope.filterFrom;
            $scope.toOpened = true;
        };
        $scope.clearFrom = function () {
            $scope.filterFrom = null;
        };
        $scope.clearTo = function () {
            $scope.filterTo = null;
        };

        // Load the data...
        $scope.loadData = function () {
            cfpLoadingBar.start();
            jogService.getJogs($scope.username).then(function (response) {
                $scope.jogs = response.data;
            }, function (response) {
                if (response.status === 404) {
                    $scope.username = null;
                }
                else {
                    $scope.error = "Something went wrong. Could not fetch the records.";
                }
            });
            cfpLoadingBar.complete();
        };
        $scope.loadData();

        $scope.openJogForm = function (jog) {
            $scope.error = "";
            $uibModal.open({
                templateUrl: '/app/views/jogForm.html',
                controller: 'jogFormController',
                resolve: {
                    jog: function () {
                        return jog ? jog : { username: $scope.username };
                    }
                }
            }).result.then(function (data) {
                if (jog) { // update
                    Object.assign(jog, data);
                }
                else { // insert
                    $scope.jogs.push(data);
                }
            }, function () {
                // Modal dismissed; do nothing.
            });
        };

        $scope.delete = function (jog) {
            commonService.showConfirmDialog('Confirm!', 'Are you sure you want to delete this record?')
            .then(function () {
                $scope.error = "";
                var index = $scope.jogs.indexOf(jog);

                jogService.deleteJog(jog.jogId).then(function (response) {
                    $scope.jogs.splice(index, 1);
                },
                function (response) {
                    if (response.status === 404) {
                        $scope.error = "Record not found.";
                    }
                    else {
                        $scope.error = "Something went wrong. The record could not be deleted.";
                    }
                });
            }, function () {
                // Cancelled; do nothing.
            });
        };
    }]);
}());