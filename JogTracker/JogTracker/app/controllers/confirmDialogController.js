﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('confirmDialogController', ["$scope", "$uibModalInstance", "params", function ($scope, $uibModalInstance, params)
    {
        $scope.title = params.title;
        $scope.message = params.message;

        $scope.ok = function () {
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }]);
}());