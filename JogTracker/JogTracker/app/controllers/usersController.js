﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.controller('usersController',
        ["$scope", "$uibModal", "cfpLoadingBar", "commonService", "authService", "userService",
        function ($scope, $uibModal, cfpLoadingBar, commonService, authService, userService)
    {
        $scope.error = "";
        $scope.users = [];
        $scope.isAdmin = authService.currentUser.role === 'Admin';

        // Sort
        $scope.sortKey = 'email';
        $scope.reverse = false;
        $scope.sort = function (key) {
            if ($scope.sortKey === key) {
                $scope.reverse = !$scope.reverse;
            }
            else {
                $scope.reverse = false;
            }
            $scope.sortKey = key;
        };

        // Load the data...
        $scope.loadData = function () {
            cfpLoadingBar.start();
            userService.getUsers().then(function (response) {
                $scope.users = response.data;
            }, function (response) {
                $scope.error = "Something went wrong. The record could not be deleted.";
            });
            cfpLoadingBar.complete();
        };
        $scope.loadData();


        $scope.openUserForm = function (user) {
            $scope.error = "";
            $uibModal.open({
                templateUrl: '/app/views/userForm.html',
                controller: 'userFormController',
                resolve: {
                    user: function () {
                        return user ? user : { role: "User" };
                    }
                }
            }).result.then(function (data) {
                if (user) { // update
                    Object.assign(user, data);
                }
                else { // insert
                    $scope.users.push(data);
                }
            }, function () {
                // Modal dismissed; do nothing.
            });
        };

        $scope.delete = function (user) {
            commonService.showConfirmDialog('Confirm!', 'Are you sure you want to delete this record?')
            .then(function () {
                $scope.error = "";
                var index = $scope.users.indexOf(user);

                userService.deleteUser(user.userId).then(function (response) {
                    $scope.users.splice(index, 1);
                },
                function (response) {
                    if (response.status === 404) {
                        $scope.error = "Record not found.";
                    }
                    else {
                        $scope.error = "Something went wrong. The record could not be deleted.";
                    }
                });
            }, function () {
                // Cancelled; do nothing.
            });
        };

        $scope.openSetPasswordForm = function (userId) {
            $scope.error = "";
            $uibModal.open({
                templateUrl: '/app/views/setPasswordForm.html',
                controller: 'setPasswordFormController',
                resolve: {
                    userId: function () {
                        return userId;
                    }
                }
            }).result.then(function (data) {
                // Do nothing...
            }, function () {
                // Modal dismissed; do nothing.
            });
        };
    }]);
}());