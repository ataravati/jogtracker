﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.filter('average', function () {
        return function (items, property) {
            var properties = items.map(function (item)
            {
                return item[property];
            });

            var sum = 0;
            for(var i = 0; i < properties.length; i++) {
                sum += properties[i];
            }
            var average = sum / properties.length;

            return average;
        };
    });
}());