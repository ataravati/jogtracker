﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.filter('dateRange', ["moment", function (moment) {
        return function (items, from, to) {
            if (!from && !to)
                return items;

            var filtered = [];
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var date = moment(item.date, 'YYYY-MM-DD').toDate();
                if ((!from || moment(date).isSameOrAfter(from)) && (!to || moment(date).isSameOrBefore(to))) {
                    filtered.push(item);
                }
            }

            return filtered;
        };
    }]);
}());