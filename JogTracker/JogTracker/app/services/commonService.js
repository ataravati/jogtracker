﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.factory("commonService", ["$uibModal", "$q", function ($uibModal, $q)
    {
        var handleError = function (response) {
            var errors = [];
            if (response.status === 400) { // Bad Request
                if (response.data.modelState) {
                    for (var key in response.data.modelState) {
                        for (var i = 0; i < response.data.modelState[key].length; i++) {
                            errors.push(response.data.modelState[key][i]);
                        }
                    }
                }
            }
            else if (response.status === 409) { // Conflict
                errors.push("An entry already exists for this date.");
            }
            else {
                errors.push("Something went wrong.");
            }

            return errors;
        };

        var showConfirmDialog = function (title, message) {
            var defer = $q.defer();

            var modalInstance = $uibModal.open({
                size: "sm",
                templateUrl: 'app/views/confirmDialog.html',
                controller: 'confirmDialogController',
                resolve: {
                    params: function () {
                        return {
                            title: title,
                            message: message
                        };
                    }
                }
            }).result.then(function (response) {
                defer.resolve(response);
            }, function (response) {
                defer.reject(response);
            });

            return defer.promise;
        };

        return {
            handleError: handleError,
            showConfirmDialog: showConfirmDialog
        };
    }]);
}());