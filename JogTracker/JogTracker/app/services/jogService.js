﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.factory("jogService", ["$http", "appSettings", function ($http, appSettings)
    {
        var baseUri = appSettings.apiServiceBaseUri;

        var getJogs = function (username) {
            return $http.get(baseUri + "api/Jogs?username=" + username);
        };

        var addJog = function (jog) {
            return $http.post(baseUri + "api/Jogs", jog);
        };

        var updateJog = function (jog) {
            return $http.put(baseUri + "api/Jogs/" + jog.jogId, jog);
        };

        var deleteJog = function (jogId) {
            return $http.delete(baseUri + "api/Jogs/" + jogId);
        };

        return {
            getJogs: getJogs,
            addJog: addJog,
            updateJog: updateJog,
            deleteJog: deleteJog
        };
    }]);
}());