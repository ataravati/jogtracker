﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.factory("authService", ["$http", "$q", "$sessionStorage", "appSettings", function ($http, $q, $sessionStorage, appSettings)
    {
        var baseUri = appSettings.apiServiceBaseUri;

        var currentUser = {
            isAuthenticated: false,
            username: '',
            role: ''
        };

        var register = function (userInfo) {
            logout();

            return $http.post(baseUri + "api/Account/Register", userInfo);
        };

        var login = function (loginInfo) {
            logout();

            var data = "grant_type=password&username=" + encodeURIComponent(loginInfo.username)
                + "&password=" + encodeURIComponent(loginInfo.password);
            var headers = { 'Content-Type': 'application/x-www-form-urlencoded' }; 

            var deferred = $q.defer();
            $http.post(baseUri + 'token', data, { headers: headers }).then(function (response)
            {
                $sessionStorage.authData = { username: response.data.userName, token: response.data.access_token };

                fillAuthData();

                deferred.resolve(response);
            }, function (response)
            {
                logout();
                deferred.reject(response.data);
            });

            return deferred.promise;
        };

        var logout = function () {
            delete $sessionStorage.authData;

            currentUser.isAuthenticated = false;
            currentUser.username = '';
            currentUser.role = '';
        };

        var fillAuthData = function () {
            var authData = $sessionStorage.authData;
            if (authData) {
                currentUser.isAuthenticated = true;
                currentUser.username = authData.username;
                fillUserRole();
            }
        };

        var fillUserRole = function () {
            $http.get(baseUri + "api/Account/GetUserRole").then(function (response) {
                currentUser.role = response.data;
            });
        };

        return {
            currentUser: currentUser,
            register: register,
            login: login,
            logout: logout,
            fillAuthData: fillAuthData
        };
    }]);
}());