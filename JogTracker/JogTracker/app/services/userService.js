﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.factory("userService", ["$http", "appSettings", function ($http, appSettings)
    {
        var baseUri = appSettings.apiServiceBaseUri;

        var getUsers = function () {
            return $http.get(baseUri + "api/Account/GetUsers");
        };

        var addUser = function (user) {
            return $http.post(baseUri + "api/Account/AddUser?role=" + user.role, user);
        };

        var updateUser = function (user) {
            return $http.put(baseUri + "api/Account/UpdateUser/" + user.userId, user);
        };

        var deleteUser = function (userId) {
            return $http.delete(baseUri + "api/Account/DeleteUser/" + userId);
        };

        var setPassword = function (data) {
            return $http.post(baseUri + "api/Account/SetPassword/" + data.userId, data);
        };

        var changePassword = function (data) {
            return $http.post(baseUri + "api/Account/ChangePassword/", data);
        };

        return {
            getUsers: getUsers,
            addUser: addUser,
            updateUser: updateUser,
            deleteUser: deleteUser,
            setPassword: setPassword,
            changePassword: changePassword
        };
    }]);
}());