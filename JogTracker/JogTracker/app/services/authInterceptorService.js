﻿(function () {
    'use strict';

    var module = angular.module("jogTrackerApp");
    module.factory('authInterceptorService', ["$q", "$injector", "$location", "$sessionStorage", function ($q, $injector, $location, $sessionStorage)
    {
        var request = function (config) {
            config.headers = config.headers || {};

            var authData = $sessionStorage.authData;
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        };

        var responseError = function (response) {
            if (response.status === 401) {
                var authService = $injector.get('authService');
                var authData = $sessionStorage.authData;

                if (authData) {
                    return $q.reject(response);
                }

                authService.logout();
                $location.path('/login');
            }

            return $q.reject(response);
        };

        return {
            request: request,
            responseError: responseError
        };
    }]);
}());